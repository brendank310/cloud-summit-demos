#!/usr/bin/env bash

set -ex

# create and export the container
docker build -t docker-demo .
docker create --name=docker-demo docker-demo
docker export docker-demo -o docker-demo.tar

# create the disk image
ROOTFS_MOUNT_POINT="$(mktemp -d)"
truncate -s 1G docker-demo.ext4
mkfs.ext4 docker-demo.ext4

# mount the disk image and add the contents of the container
sudo mount docker-demo.ext4 "${ROOTFS_MOUNT_POINT}"
sudo chown ${USER}:${USER} -R "${ROOTFS_MOUNT_POINT}"
tar -xf docker-demo.tar -C "${ROOTFS_MOUNT_POINT}"

# unmount and cleanup
sudo umount "${ROOTFS_MOUNT_POINT}"
rm -rf "${ROOTFS_MOUNT_POINT}"
