#!/usr/bin/env bash

docker stop $(docker ps -aq)
docker rm $(docker ps -aq) --force
docker rmi $(docker images -aq) --force

rm docker-demo.ext4 docker-demo.tar
rm -rf /tmp/tmp.*
